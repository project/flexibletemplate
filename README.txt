-- SUMMARY --

Allows you to create textfield and textarea elements in editable area, so that another editor will be able edit just this textfields and textareas and not will be able edit all the text.

-- REQUIREMENTS --

-- RECOMMENDED --

The wysiwyg with the tinymce

-- INSTALLATION --

* Switch on module on page modules

Install the wysiwyg with the tinymce, add copy in sites\all\libraries\tinymce\jscripts\tiny_mce\plugins the folder "flexibletemplate" from the module directory. After this action will available for use the plugin tinymce.

Check "Insert flexible template" on the page profile, than activate this the plugin.

* On page admin/user/permissions check "edit full template" for those users, which will insert the form element in text and "edit share template" for those, which will be able just insert the text in inserted form elements.

-- USE --

1. Create node and insert in the text, example, <txtfield id_name="396" size="12">test text</txtfield>. Users must be able "edit full template"
2. Edit this node user, which have permission "edit share template"

-- CONTACT --

Developer http://drupal.org/user/203339
This project has been sponsored by:
* JASMiND
  Is an independent professional consulting group with extensive background in telecommunications, IT, software development and implementation. Visit http://jmproduct.ru/en for more information.