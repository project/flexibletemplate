tinyMCEPopup.requireLangPack();

var FlexibleTemplateDialog = {
	preInit : function() {

	},

	init : function() {
		
	},

	resize : function() {
		var w, h, e;

		if (!self.innerWidth) {
			w = document.body.clientWidth - 50;
			h = document.body.clientHeight - 160;
		} else {
			w = self.innerWidth - 50;
			h = self.innerHeight - 170;
		}

		e = document.getElementById('flexibletemplatesrc');

		if (e) {
			e.style.height = Math.abs(h) + 'px';
			e.style.width  = Math.abs(w - 5) + 'px';
		}
	},

	selectTemplate : function(u, ti) {
		if (!u)
			this.selectElement = null;
		this.selectElement = u;
	},
	
	sizeTemplate: function(u, ti) {
		if (!u)
			this.sizeElement = '';;
		this.sizeElement = u;
	},
	
	valueTemplate: function(u, ti) {
		if (!u)
			this.valueElement = '';;
		this.valueElement = u;
	},
	
	requiredTemplate: function(u, ti) {
		if (!u)
			this.requiredElement = false;
		this.requiredElement = u;
	},
	
	nameTemplate: function(u, ti) {
		if (!u)
			this.nameElement = '';
		this.nameElement = u;
	},
	
	loadCSSFiles : function(d) {
		var ed = tinyMCEPopup.editor;

		tinymce.each(ed.getParam("content_css", '').split(','), function(u) {
			d.write('<link href="' + ed.documentBaseURI.toAbsolute(u) + '" rel="stylesheet" type="text/css" />');
		});
	},

 	insert : function() {
		if (typeof(this.selectElement) == 'undefined' || this.selectElement == null || this.selectElement == '') return;
		if (typeof(this.nameElement) == 'undefined' || this.nameElement == '') return;
		// сделать редактирование при клике в ту же точку + форматировать блоки параметров в таблицу
		if (typeof(this.sizeElement) != 'undefined' ){
			this.sizeElement = 'size="'+this.sizeElement+'"';
		}else {
			this.sizeElement = '';
		}

		if (typeof(this.requiredElement) == "boolean" && this.requiredElement == true){
			this.requiredElement = 'required="'+this.requiredElement+'"';
			//alert(this.requiredElement);
		}else {
			this.requiredElement = '';
		}
		
		if (typeof(this.valueElement) == 'undefined' ){
			this.valueElement = '';
		}

		tinyMCEPopup.execCommand('mceInsertFlexibletemplate', false, {
			content : '<'+this.selectElement+' id_name="'+this.nameElement+'" '+this.requiredElement+' '+this.sizeElement+'>'+this.valueElement+'</'+this.selectElement+'>',
			selection : tinyMCEPopup.editor.selection.getContent({format : 'text'})
		});

		tinyMCEPopup.close();
	},	
};

FlexibleTemplateDialog.preInit();
tinyMCEPopup.onInit.add(FlexibleTemplateDialog.init, FlexibleTemplateDialog);